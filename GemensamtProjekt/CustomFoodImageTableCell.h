//
//  CustomFoodImageTableCell.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-26.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFoodImageTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *foodNameText;
@property (weak, nonatomic) IBOutlet UIImageView *foodImage;

@end
