//
//  AppDelegate.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-23.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

