//
//  FoodInfo.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-04.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodInfo : NSObject <NSCoding>
@property (nonatomic, readonly) NSMutableArray *nutrientInfo;
@property (nonatomic) NSString *quickInfoType;
@property (nonatomic, readonly) NSDictionary *activeQuickTypeDictionary;

// Graph Settings
@property (nonatomic, readonly) NSInteger numberOfBars;
@property (nonatomic, readonly) NSArray *nutrientsToCompare;
@property (nonatomic) NSMutableArray *foodsToCompare;

-(void)loadNutrientsInfo;
-(NSDictionary*)getFoodInfoForSlug:(NSString*)slug;

@end
