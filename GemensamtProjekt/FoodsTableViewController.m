//
//  FoodsTableViewController.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-24.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "FoodsTableViewController.h"
#import "CustomFoodsTableCell.h"
#import "FoodDetailTableViewController.h"
#import "DataManager.h"
#import "Food.h"
#import "FoodInfo.h"

@interface FoodsTableViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *showFavouritesButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *compareButton;

@property (nonatomic) NSArray *foods;
@property (nonatomic) NSArray *searchResult;
@property (nonatomic) UISearchController *searchController;
@property (nonatomic) DataManager *dataManager;
@property (nonatomic) BOOL favouriteActive;
@property (nonatomic) NSArray *favourites;
@property (nonatomic) NSArray *tableArray;
@property (nonatomic) NSDictionary *quickNutrientInfo;
@property (nonatomic) UIActivityIndicatorView *indicator;
@property (nonatomic) UIView *waitingForData;
@property (nonatomic) NSTimer *checkDataTimer;
@property (nonatomic) BOOL isComparing;
@property (nonatomic) int nComparing;

@end

@implementation FoodsTableViewController

-(DataManager*)dataManager {
    if (!_dataManager) {
        _dataManager = [DataManager getInstance];
    }
    return _dataManager;
}

-(NSArray*)foods{
    if(!_foods){
        _foods = self.dataManager.foodsArray;
    }
    return _foods;
}

-(NSDictionary*)quickNutrientInfo {
    if (!_quickNutrientInfo) {
        _quickNutrientInfo = self.dataManager.foodInfo.activeQuickTypeDictionary;
    }
    return _quickNutrientInfo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = false;
    self.definesPresentationContext = YES;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
}

-(void)viewWillAppear:(BOOL)animated {
    [self bookmarkButtonLooks];
    self.favourites = self.dataManager.favourites;
    if (self.favourites.count < 1) {
        self.favouriteActive = NO;
    }
    self.quickNutrientInfo = self.dataManager.foodInfo.activeQuickTypeDictionary;
    self.dataManager.foodInfo.foodsToCompare = @[].mutableCopy;
    [self checkBarButtonTints];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0){
        return self.searchResult.count;
    } else if (self.favouriteActive) {
        return self.favourites.count;
    } else {
        return self.foods.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomFoodsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customFoodsCell"
                                                            forIndexPath:indexPath];
    BOOL isPriorityLoading = NO;
    if (self.searchController.isActive && self.searchController.searchBar.text.length > 0){
        self.tableArray = self.searchResult;
    } else if (self.favouriteActive) {
        self.tableArray = self.favourites;
    } else {
        self.tableArray = self.foods;
    }
    if (indexPath.row == 0 && ![self.dataManager isDataReadyToDisplayForArray:self.tableArray]) {
        [self.checkDataTimer invalidate];
        [self setupLoadingView];
        self.checkDataTimer = [NSTimer scheduledTimerWithTimeInterval:1/2 target:self selector:@selector(checkArrayReady:) userInfo:self.tableArray repeats:YES];
        isPriorityLoading = YES;
    }
    [self.dataManager shouldStartLoadNutrientsForArray:self.tableArray atIndex:indexPath.row isPriority:isPriorityLoading];
   
    Food *food = self.tableArray[indexPath.row];
    cell.nameText.text = food.name;
    cell.energyLabel.text = [NSString stringWithFormat:@"%@: %@ %@" , self.quickNutrientInfo[@"name"], food.nutrients[self.dataManager.foodInfo.quickInfoType], self.quickNutrientInfo[@"unit"]];
    if (indexPath.row > 50 && ![self.dataManager isDataReadyToDisplayForArray:[self.tableArray subarrayWithRange:NSMakeRange(indexPath.row, 1)]]) {
        [self.checkDataTimer invalidate];
        self.checkDataTimer = [NSTimer scheduledTimerWithTimeInterval:1/2 target:self selector:@selector(checkIfDisplayReady:) userInfo:[self.tableArray subarrayWithRange:NSMakeRange(indexPath.row, 1)] repeats:YES];
        cell.energyLabel.text = [NSString stringWithFormat:@"%@: Loading..." , self.quickNutrientInfo[@"name"]];
    } else if ([self.dataManager hasNutrientForFood:food withSlug:self.dataManager.foodInfo.quickInfoType]) {
        cell.energyLabel.text = [NSString stringWithFormat:@"%@: %@ %@" , self.quickNutrientInfo[@"name"], food.nutrients[self.dataManager.foodInfo.quickInfoType], self.quickNutrientInfo[@"unit"]];
    } else {
        cell.energyLabel.text = [NSString stringWithFormat:@"%@: N/A" , self.quickNutrientInfo[@"name"]];
    }
    cell.healthyValue.text = [NSString stringWithFormat:@"Nyttighet: %@", [self.dataManager foodHealthValue:food]];
    cell.foodImage.image = food.foodImage;
    cell.nameText.selectable = NO;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isComparing) {
        [self.dataManager.foodInfo.foodsToCompare addObject:self.tableArray[indexPath.row]];
        if (self.dataManager.foodInfo.foodsToCompare.count >= 2) {
            [self performSegueWithIdentifier:@"compareSegue" sender:self];
            self.isComparing = NO;
        }
    }
}

- (IBAction)showFavouritesButtonAction:(id)sender {
    self.favourites = self.dataManager.favourites;
    if (self.favourites.count > 0) {
        self.favouriteActive = !self.favouriteActive;
        [self.tableView reloadData];
    }
    [self bookmarkButtonLooks];
}
- (IBAction)compareButton:(id)sender {
    self.isComparing = !self.isComparing;
    [self checkBarButtonTints];
}

-(void)checkBarButtonTints {
    if (self.isComparing) {
        self.compareButton.title = [NSString stringWithFormat:@"Jämför(%d/2)", self.nComparing++];
        [self.compareButton setTintColor:[UIColor greenColor]];
        [self.compareButton setStyle:UIBarButtonItemStyleDone];
    } else {
        self.compareButton.title = @"Jämför";
        self.nComparing = 0;
        [self.compareButton setTintColor:[UIColor redColor]];
        [self.compareButton setStyle:UIBarButtonItemStylePlain];
        self.dataManager.foodInfo.foodsToCompare = @[].mutableCopy;
    }
}

-(void)bookmarkButtonLooks {
    if (self.favouriteActive){
        [self.showFavouritesButton setTintColor:[UIColor greenColor]];
        [self.showFavouritesButton setStyle:UIBarButtonItemStyleDone];
    } else {
        [self.showFavouritesButton setTintColor:[UIColor redColor]];
        [self.showFavouritesButton setStyle:UIBarButtonItemStylePlain];
    }
}

-(void)setupLoadingView {
    self.waitingForData = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.waitingForData.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:self.waitingForData];
    
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.waitingForData addSubview:self.indicator];
    self.indicator.center = self.view.center;
    [self.indicator startAnimating];
    self.tableView.scrollEnabled = NO;
}

-(void)removeLoadingView {
    [self.checkDataTimer invalidate];
    self.checkDataTimer = nil;
    [self.indicator stopAnimating];
    [self.indicator removeFromSuperview];
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.waitingForData.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         [self.tableView reloadData];
                         self.waitingForData.alpha = 1.0f;
                         [self.waitingForData removeFromSuperview];
                         self.tableView.scrollEnabled = YES;
                     }];
    [self.tableView reloadData];
}

-(void)checkArrayReady:(NSArray*)data {
    if ([self.dataManager isDataReadyToDisplayForArray:self.tableArray]) {
        [self removeLoadingView];
    }
}

-(void)checkIfDisplayReady:(NSArray*)data {
    if ([self.dataManager isDataReadyToDisplayForArray:self.tableArray]) {
        [self.checkDataTimer invalidate];
        self.checkDataTimer = nil;
        [self.tableView reloadData];
    }
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSString *searchText = searchController.searchBar.text;
    NSPredicate *result = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    self.searchResult = [self.foods filteredArrayUsingPredicate:result];
    [self removeLoadingView];
    [self.tableView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"foodstuffDetailsSegue"]) {
        FoodDetailTableViewController *foodDetail = [segue destinationViewController];
        UITableViewCell *cell = sender;
        NSInteger index = [self.tableView indexPathForCell:cell].row;
        Food *food = self.tableArray[index];
        foodDetail.food = food;
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"foodstuffDetailsSegue"] && self.isComparing) {
        [self checkBarButtonTints];
        return NO;
    }
    return YES;
}

@end
