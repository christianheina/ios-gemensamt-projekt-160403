//
//  Food.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-03.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIImage.h"

@interface Food : NSObject <NSCoding>
@property (nonatomic, readonly) NSNumber *index;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic) BOOL isFavourite;
@property (nonatomic, readonly) NSDictionary *nutrients;
@property (nonatomic) UIImage *foodImage;

-(instancetype)initWithName:(NSString*)name andNumber:(NSNumber*)index;
-(void)getDataFromApi:(long)priorityIdentifier;
-(void)clearNutrientData;
-(void)checkName:(NSString*)nameToCompare;

@end
