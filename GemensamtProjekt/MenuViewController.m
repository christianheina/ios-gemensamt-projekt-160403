//
//  SplashViewController.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-26.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()
@property (weak, nonatomic) IBOutlet UIButton *foodsButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIDynamicItemBehavior *elasticity;

@property (nonatomic) NSMutableArray *physicsItems;
@property (nonatomic) NSTimer *itemsTimer;

@end

@implementation MenuViewController

-(NSMutableArray*)physicsItems {
    if (!_physicsItems) {
        _physicsItems = @[].mutableCopy;
    }
    return _physicsItems;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [self addPhysics];
}

-(void)addItem {
    if (self.physicsItems.count < 10)
        for (int i = 0; i < 2; i++) {
            UIView *view = [self getPhysicsItem];
            [self.physicsItems addObject:view];
        }
    NSArray *array = self.physicsItems.copy;
    for (UIView *view in array) {
        if (view.frame.origin.y+view.frame.size.height >= 16*self.view.frame.size.height/20) {
            [self.physicsItems removeObject:view];
            [UIView animateWithDuration:0.2
                             animations:^{
                                 view.alpha = 0.0;
                             }
                             completion:^(BOOL finished) {
                                 [view removeFromSuperview];
                             }];
        }
    }
    [self updatePhysics];
    self.foodsButton.layer.zPosition = 1;
    self.settingsButton.layer.zPosition = 1;
}

-(void)updatePhysics {
    [self.animator removeBehavior:self.gravity];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:self.physicsItems];
    [self.animator addBehavior:self.gravity];
    
    [self.animator removeBehavior:self.collision];
    self.collision = [[UICollisionBehavior alloc] initWithItems:self.physicsItems];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
    
    [self.animator removeBehavior:self.elasticity];
    self.elasticity = [[UIDynamicItemBehavior alloc] initWithItems:self.physicsItems];
    self.elasticity.elasticity = 0.5f;
    [self.animator addBehavior:self.elasticity];
}

-(void)addPhysics {
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.itemsTimer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(addItem) userInfo:nil repeats:YES];
}

-(UIView*)getPhysicsItem {
    int startX = arc4random_uniform(self.view.frame.size.width);
    int startY = arc4random_uniform(self.view.frame.size.height/3);
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(startX, startY, 32, 32)];
    float red = arc4random()%10/10.0;
    float blue = arc4random()%10/10.0;
    float green = arc4random()%10/10.0;
    float rotation = arc4random()%6/6.0;
    view.transform = CGAffineTransformMakeRotation(rotation);
    view.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    [self.view addSubview:view];
    return view;
}

-(void)stopPhysics {
    [self.itemsTimer invalidate];
    self.itemsTimer = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self stopPhysics];
}


@end
