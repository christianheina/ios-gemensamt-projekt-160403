//
//  GraphViewController.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-17.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "GraphViewController.h"
#import "DataManager.h"

@interface GraphViewController ()
@property (weak, nonatomic) IBOutlet GKBarGraph *graphView;

@property (weak, nonatomic) IBOutlet UILabel *firstItemLabel;
@property (weak, nonatomic) IBOutlet UIView *firstItemColorView;

@property (weak, nonatomic) IBOutlet UILabel *secondItemLabel;
@property (weak, nonatomic) IBOutlet UIView *secondItemColorView;

@property (weak, nonatomic) IBOutlet UILabel *firstCompareItem;
@property (weak, nonatomic) IBOutlet UILabel *secondCompareItem;
@property (weak, nonatomic) IBOutlet UILabel *thirdCompareItem;

@property (nonatomic) DataManager *dataManager;

@end

@implementation GraphViewController

-(DataManager*)dataManager {
    if (!_dataManager) {
        _dataManager = [DataManager getInstance];
    }
    return _dataManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    Food *food;
    
    food = self.dataManager.foodInfo.foodsToCompare[0];
    self.firstItemLabel.text = food.name;
    self.firstItemColorView.backgroundColor = [UIColor redColor];
    
    food = self.dataManager.foodInfo.foodsToCompare[1];
    self.secondItemLabel.text = food.name;
    self.secondItemColorView.backgroundColor = [UIColor blueColor];
    
    self.firstCompareItem.text = [NSString stringWithFormat:@"1. %@", self.dataManager.foodInfo.nutrientsToCompare[0][@"name"]];
    self.secondCompareItem.text = [NSString stringWithFormat:@"2. %@", self.dataManager.foodInfo.nutrientsToCompare[1][@"name"]];
    self.thirdCompareItem.text = [NSString stringWithFormat:@"3. %@", self.dataManager.foodInfo.nutrientsToCompare[2][@"name"]];
    
    self.graphView.dataSource = self;
    [self.graphView draw];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSInteger)numberOfBars {
    return self.dataManager.foodInfo.numberOfBars;
}

- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    Food *food = self.dataManager.foodInfo.foodsToCompare[index % 2];
    NSDictionary *currentNutrient = self.dataManager.foodInfo.nutrientsToCompare[(index - (index % 2))/2];
    return food.nutrients[currentNutrient[@"slug"]];
}

-(NSString *)titleForBarAtIndex:(NSInteger)index {
    return [NSString stringWithFormat:@"%ld", (1+(index - (index % 2))/2)];
}

-(UIColor *)colorForBarAtIndex:(NSInteger)index {
    return @[[UIColor redColor], [UIColor blueColor]][index % 2];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
