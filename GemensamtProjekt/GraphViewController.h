//
//  GraphViewController.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-17.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKBarGraph.h"

@interface GraphViewController : UIViewController <GKBarGraphDataSource>

@end
