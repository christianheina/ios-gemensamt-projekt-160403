//
//  DataManager.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-03.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "DataManager.h"
#import "Food.h"
#import "Math.h"

@interface DataManager ()
@property (nonatomic) NSMutableArray *foodsArray;
@property (nonatomic) NSArray *favourites;
@property (nonatomic) BOOL isDataReadyToDisplay;
@property (nonatomic) FoodInfo *foodInfo;
@property (nonatomic) Settings *settings;
@property (nonatomic) long lastApiUpdate;
@property (nonatomic) NSTimer *saveTimer;

@end

@implementation DataManager

static NSString *foodApi = @"http://matapi.se/foodstuff";
static NSString *foodsKey = @"foodsKey";
static NSString *foodInfoKey = @"foodInfoKey";
static NSString *settingsKey = @"settingsKey";
static NSString *lastApiUpdateKey = @"lastApiUpdate";
static NSString *timeBetweenSavesKey = @"timeBetweenSaves";
static DataManager *instance = nil;

+(id)getInstance {
    if (instance == nil){
        instance = [[self alloc] init];
    }
    return instance;
}

-(id)init {
    self = [super init];
    return self;
}

-(BOOL)isDataReadyToDisplay {
    Food *food = self.foodsArray[40];
    if ([[NSString stringWithFormat:@"%@", food.nutrients[@"energyKcal"]] isEqualToString:@"(null)"]) {
        return NO;
    }
    return YES;
}

-(FoodInfo*)foodInfo {
    if (!_foodInfo){
        _foodInfo = [self loadFoodInfoData];
    }
    return _foodInfo;
}

-(Settings*)settings {
    if (!_settings) {
        _settings = [self loadSettingsData];
    }
    return _settings;
}

-(NSMutableArray*)foodsArray {
    if (!_foodsArray) {
        _foodsArray = [self loadFoodsData];
    }
    return _foodsArray;
}

-(NSArray*)favourites {
    NSPredicate *favs = [NSPredicate predicateWithFormat:@"isFavourite == YES"];
    _favourites = [self.foodsArray filteredArrayUsingPredicate:favs];
    return _favourites;
}

-(long)lastApiUpdate {
    if (!_lastApiUpdate) {
        _lastApiUpdate = [self loadLastApiUpdate];
    }
    return _lastApiUpdate;
}

-(NSString*)foodHealthValue:(Food*)food {
    if ([self hasNutrientForFood:food withSlug:@"energyKcal"]){
        return [NSString stringWithFormat:@"%.0f", 100*([food.nutrients[@"energyKcal"] floatValue]/2000)];
    }
    return @"N/A";
}

-(BOOL)hasNutrientForFood:(Food*)food withSlug:(NSString*)slug {
    NSString *temp = [NSString stringWithFormat:@"%@", food.nutrients[slug]];
    if ([temp isEqualToString:@"(null)"]) {
        return NO;
    }
    return YES;
}

-(void)getFoodsApiData {
    NSURL *url = [NSURL URLWithString:foodApi];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if (error) {
            return;
        }
        
        NSError *parseError;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            if (self.foodsArray.count == 0){
                for (NSDictionary *dict in json){
                    Food *food = [[Food alloc] initWithName:dict[@"name"] andNumber:dict[@"number"]];
                    [self.foodsArray addObject:food];
                }
            } else {
                for (NSUInteger i = 0, count = json.count; i < count; i++){
                    Food *food = self.foodsArray[i];
                    NSDictionary *dict = json[i];
                    if (food.index == dict[@"number"]){
                        [food checkName:dict[@"name"]];
                    } else {
                        [self checkInFoodsArray:dict];
                    }
                }
            }
            
        });
    }];
    [task resume];
}

-(void)shouldStartLoadNutrientsForArray:(NSArray*)tableArray atIndex:(NSInteger)rowIndex isPriority:(BOOL)priority {
    if (!priority && !rowIndex == 0) {
        if (tableArray.count-rowIndex > 300 && rowIndex%100 == 0) {
            [self loadNutrientsForSubArray:[tableArray subarrayWithRange:NSMakeRange(rowIndex+100, 200)] withGCDPriority:DISPATCH_QUEUE_PRIORITY_DEFAULT];
        } else if (rowIndex%100 == 0 && tableArray.count-rowIndex > 100){
            [self loadNutrientsForSubArray:[tableArray subarrayWithRange:NSMakeRange(rowIndex+100, tableArray.count-(rowIndex+100))] withGCDPriority:DISPATCH_QUEUE_PRIORITY_DEFAULT];
        }
    } else if (priority){
        if (tableArray.count > 50) {
            [self loadNutrientsForSubArray:[tableArray subarrayWithRange:NSMakeRange(0, 50)] withGCDPriority:DISPATCH_QUEUE_PRIORITY_HIGH];
            if (tableArray.count > 200) {
                [self loadNutrientsForSubArray:[tableArray subarrayWithRange:NSMakeRange(rowIndex+50, 150)] withGCDPriority:DISPATCH_QUEUE_PRIORITY_LOW];
            } else {
                [self loadNutrientsForSubArray:[tableArray subarrayWithRange:NSMakeRange(rowIndex+50, tableArray.count-50)] withGCDPriority:DISPATCH_QUEUE_PRIORITY_LOW];
            }
        } else {
            [self loadNutrientsForSubArray:[tableArray subarrayWithRange:NSMakeRange(rowIndex, tableArray.count)] withGCDPriority:DISPATCH_QUEUE_PRIORITY_HIGH];
        }
    }

}

-(void)loadNutrientsForSubArray:(NSArray*)subArray withGCDPriority:(long)priorityIdentifier{
    long currentTime = CFAbsoluteTimeGetCurrent();
    for (Food *food in subArray) {
        if (![self hasNutrientForFood:food withSlug:@"energyKcal"] || currentTime - [self lastApiUpdate] > self.settings.updateIntervalMilliseconds) {
            [food getDataFromApi:priorityIdentifier];
        }
    }
}

-(BOOL)isDataReadyToDisplayForArray:(NSArray*)data {
    if (data.count > 40) {
        data = [data subarrayWithRange:NSMakeRange(0, 40)];
    } else {
        data = [data subarrayWithRange:NSMakeRange(0, data.count)];
    }
    for (Food *food in data) {
        if (![self hasNutrientForFood:food withSlug:@"energyKcal"]) {
            return NO;
        }
    }
    return YES;
}

-(void)updateDataFromApi:(long)currentTime {
    [self getFoodsApiData];
    [self.foodInfo loadNutrientsInfo];
    [self saveApiUpdate:currentTime];
}

-(void)checkIfTimeToUpdate {
    long currentTime = CFAbsoluteTimeGetCurrent();
    if (currentTime - [self lastApiUpdate] > self.settings.updateIntervalMilliseconds || self.foodsArray.count < 1) {
        [self updateDataFromApi:currentTime];
    }
}

-(void)checkInFoodsArray:(NSDictionary*)foodToCheckFor {
    for (Food *food in self.foodsArray) {
        if ([foodToCheckFor[@"name"] isEqualToString:food.name] && foodToCheckFor[@"number"] == food.index) {
            return;
        }
    }
    Food *food = [[Food alloc] initWithName:foodToCheckFor[@"name"] andNumber:foodToCheckFor[@"number"]];
    [self.foodsArray addObject:food];
}

-(void)startPeriodicSaves {
    [self foodsArraySaveSync];
    self.saveTimer = [NSTimer scheduledTimerWithTimeInterval:self.settings.secondsBetweenSaves target:self selector:@selector(foodsArraySaveSync) userInfo:nil repeats:YES];
}

-(void)stopPeriodicSaves {
    [self foodsArraySaveSync];
    [self.saveTimer invalidate];
    self.saveTimer = nil;
}

-(void)resetFoodsNutrient {
    for (Food *food in self.foodsArray) {
        [food clearNutrientData];
    }
}

-(void)foodsArraySaveSync {
    [self saveFoodsData];
    [self saveFoodInfoData];
}

-(void)saveFoodsData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.foodsArray];
    [defaults setObject:data forKey:foodsKey];
    [defaults synchronize];
}

-(NSMutableArray*)loadFoodsData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:foodsKey];
    if (data) {
        return self.foodsArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    } else {
        return @[].mutableCopy;
    }
}

-(void)saveFoodInfoData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.foodInfo];
    [defaults setObject:data forKey:foodInfoKey];
    [defaults synchronize];
}

-(FoodInfo*)loadFoodInfoData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:foodInfoKey];
    if (data) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    } else {
        return [[FoodInfo alloc] init];
    }
}

-(void)saveSettingsData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.settings];
    [defaults setObject:data forKey:settingsKey];
    [defaults synchronize];
}

-(Settings*)loadSettingsData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:settingsKey];
    if (data) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    } else {
        return [[Settings alloc] init];
    }
}

-(long)loadLastApiUpdate {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults integerForKey:lastApiUpdateKey];
}

-(void)saveApiUpdate:(long)updateTime {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:updateTime forKey:lastApiUpdateKey];
    [defaults synchronize];
}

-(void)clearBookmarks {
    for (Food *food in self.foodsArray) {
        food.isFavourite = NO;
    }
}

-(void)clearPhotos {
    for (Food *food in self.foodsArray) {
        food.foodImage = nil;
    }
}

@end
