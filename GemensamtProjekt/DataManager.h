//
//  DataManager.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-03.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Food.h"
#import "FoodInfo.h"
#import "Settings.h"

@interface DataManager : NSObject
@property (nonatomic, readonly) NSMutableArray *foodsArray;
@property (nonatomic, readonly) NSArray *favourites;
@property (nonatomic, readonly) BOOL isDataReadyToDisplay;
@property (nonatomic, readonly) FoodInfo *foodInfo;
@property (nonatomic, readonly) Settings *settings;
@property (nonatomic, readonly) long lastApiUpdate;
@property (nonatomic) long timeBetweenSaves;

+(id)getInstance;

-(NSString*)foodHealthValue:(Food*)food;

-(void)updateDataFromApi:(long)currentTime;
-(void)checkIfTimeToUpdate;

-(void)startPeriodicSaves;
-(void)stopPeriodicSaves;

-(void)resetFoodsNutrient;

-(BOOL)isDataReadyToDisplayForArray:(NSArray*)data;
-(void)loadNutrientsForSubArray:(NSArray*)subArray withGCDPriority:(long)priorityIdentifier;
-(void)shouldStartLoadNutrientsForArray:(NSArray*)tableArray atIndex:(NSInteger)rowIndex isPriority:(BOOL)priority;
-(BOOL)hasNutrientForFood:(Food*)food withSlug:(NSString*)slug;

-(void)saveFoodsData;
-(void)saveFoodInfoData;
-(void)saveSettingsData;

-(void)clearPhotos;
-(void)clearBookmarks;

@end
