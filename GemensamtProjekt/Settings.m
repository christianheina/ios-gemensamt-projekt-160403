//
//  Settings.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-04.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "Settings.h"
#import "DataManager.h"
#import "Food.h"

@interface Settings ()
@property (nonatomic) NSArray *updateIntervals;
@property (nonatomic) NSArray *saveIntervals;
@property (nonatomic) NSDictionary *activeSaveDictionary;

@end

@implementation Settings

static const long millisInDay = 86400000;

-(NSInteger)daysBetweenUpdate {
    if (!_daysBetweenUpdate) {
        _daysBetweenUpdate = 1;
    }
    return _daysBetweenUpdate;
}

-(long)updateIntervalMilliseconds {
    _updateIntervalMilliseconds = self.daysBetweenUpdate*millisInDay;
    return _updateIntervalMilliseconds;
}

-(NSArray*)updateIntervals {
    if (!_updateIntervals) {
        _updateIntervals = @[@{@"name" : @"Dagligen", @"value" : @1},
                             @{@"name" : @"Veckovis", @"value" : @7},
                             @{@"name" : @"Varje månad", @"value" : @30},
                             @{@"name" : @"Varje 3 månader", @"value" : @90},
                             @{@"name" : @"Varje 6 månader", @"value" : @180},
                             @{@"name" : @"Årligen", @"value" : @365},
                             @{@"name" : @"Aldrig", @"value" : @3650}];
    }
    return _updateIntervals;
}

-(NSInteger)secondsBetweenSaves {
    if (!_secondsBetweenSaves) {
        _secondsBetweenSaves = 30;
    }
    return _secondsBetweenSaves;
}

-(NSArray*)saveIntervals {
    if (!_saveIntervals) {
        _saveIntervals = @[@{@"name" : @"Varje 15 sekunder", @"value" : @15},
                           @{@"name" : @"Varje 30 sekunder", @"value" : @30},
                           @{@"name" : @"Varje 45 sekunder", @"value" : @45},
                           @{@"name" : @"Varje 60 sekunder", @"value" : @60},
                           @{@"name" : @"Varje 120 sekunder", @"value" : @120}];
    }
    return _saveIntervals;
}

-(NSDictionary*)activeSaveDictionary {
    NSPredicate *active = [NSPredicate predicateWithFormat:@"value == %ld", self.secondsBetweenSaves];
    NSArray *array = [self.saveIntervals filteredArrayUsingPredicate:active];
    return array[0];
}

-(NSDictionary*)activeUpdateDictionary {
    NSPredicate *active = [NSPredicate predicateWithFormat:@"value == %ld", self.daysBetweenUpdate];
    NSArray *array = [self.updateIntervals filteredArrayUsingPredicate:active];
    return array[0];
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeInteger:self.daysBetweenUpdate forKey:@"settingsDaysBetweenUpdate"];
    [aCoder encodeInteger:self.secondsBetweenSaves forKey:@"settingsSecondsBetweenSaves"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    
    if (self) {
        self.daysBetweenUpdate = [aDecoder decodeIntegerForKey:@"settingsDaysBetweenUpdate"];
        self.secondsBetweenSaves = [aDecoder decodeIntegerForKey:@"settingsSecondsBetweenSaves"];
    }
    return self;
}

@end
