//
//  FoodDetailTableViewController.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-26.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "FoodDetailTableViewController.h"
#import "CustomNutrientsTableCell.h"
#import "CustomFoodImageTableCell.h"
#import "CustomFoodTableCell.h"
#import "DataManager.h"

@interface FoodDetailTableViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bookmarkButton;
@property (nonatomic) NSDictionary *nutrients;
@property (nonatomic) NSArray *nutrientInfo;
@property (nonatomic) DataManager *dataManager;

@end

@implementation FoodDetailTableViewController

-(NSDictionary*)nutrients{
    if(!_nutrients){
        _nutrients = self.food.nutrients;
    }
    return _nutrients;
}

-(DataManager*)dataManager {
    if (!_dataManager) {
        _dataManager = [DataManager getInstance];
    }
    return _dataManager;
}

-(NSArray*)nutrientInfo {
    if (!_nutrientInfo) {
        _nutrientInfo = self.dataManager.foodInfo.nutrientInfo;
    }
    return _nutrientInfo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.food.nutrients == nil) {
        [self waitForNutrients];
        [self.dataManager loadNutrientsForSubArray:@[self.food] withGCDPriority:DISPATCH_QUEUE_PRIORITY_HIGH];
    }
}

-(void)waitForNutrients {
    if (self.food.nutrients == nil) {
        [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(waitForNutrients) userInfo:nil repeats:NO];
    } else {
        [self.tableView reloadData];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [self bookmarkButtonLooks];
}

- (IBAction)takePictureButton:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)addToFavouritesButton:(id)sender {
    self.food.isFavourite = !self.food.isFavourite;
    [self.dataManager saveFoodsData];
    [self bookmarkButtonLooks];
}

-(void)bookmarkButtonLooks {
    if (self.food.isFavourite){
        [self.bookmarkButton setTintColor:[UIColor greenColor]];
        [self.bookmarkButton setStyle:UIBarButtonItemStyleDone];
    } else {
        [self.bookmarkButton setTintColor:[UIColor redColor]];
        [self.bookmarkButton setStyle:UIBarButtonItemStylePlain];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0){
        return 1;
    }
    if (self.food.nutrients == nil) {
        return 1;
    }
    return self.nutrients.count;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return @"";
    } else {
        return @"Nutrients";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        if (self.food.foodImage != nil) {
            CustomFoodImageTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customFoodImageCell" forIndexPath:indexPath];
            cell.foodNameText.text = self.food.name;
            cell.foodImage.image = self.food.foodImage;
            return cell;
        } else {
            CustomFoodTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customFoodCell" forIndexPath:indexPath];
            cell.foodNameText.text = self.food.name;
            return cell;
        }
    } else if (self.food.nutrients == nil) {
        CustomNutrientsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customNutrientsCell" forIndexPath:indexPath];
        
        cell.nutrientNameText.text = @"Data not ready, please wait";
        cell.nutrientValueText.text = @"";
        return cell;
    } else {
        CustomNutrientsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customNutrientsCell" forIndexPath:indexPath];
        NSDictionary *dict = self.nutrientInfo[indexPath.row];
        
        cell.nutrientNameText.text = dict[@"name"];
        if ([self.dataManager hasNutrientForFood:self.food withSlug:dict[@"slug"]]) {
            cell.nutrientValueText.text = [NSString stringWithFormat:@"%.2f %@", [self.nutrients[dict[@"slug"]] floatValue], dict[@"unit"]];
        } else {
            cell.nutrientValueText.text = @"N/A";
        }
        
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CustomFoodImageTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customFoodImageCell" forIndexPath:indexPath];
        return cell.foodImage.frame.size.height+cell.foodNameText.frame.size.height+24;
    } else {
        CustomNutrientsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customNutrientsCell" forIndexPath:indexPath];
        return cell.nutrientNameText.frame.size.height+8;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.food.foodImage = info[UIImagePickerControllerEditedImage];
    [self.dataManager saveFoodsData];
    [self.tableView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
