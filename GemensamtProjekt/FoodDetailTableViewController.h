//
//  FoodDetailTableViewController.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-26.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Food.h"

@interface FoodDetailTableViewController : UITableViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic) Food *food;

@end
