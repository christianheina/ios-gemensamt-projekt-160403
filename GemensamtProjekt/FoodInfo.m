//
//  FoodInfo.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-04.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "FoodInfo.h"
#import "DataManager.h"

@interface FoodInfo ()
@property (nonatomic) NSMutableArray *nutrientInfo;
@property (nonatomic) DataManager *dataManager;
@property (nonatomic) NSDictionary *activeQuickTypeDictionary;

@property (nonatomic) NSInteger numberOfBars;
@property (nonatomic) NSArray *nutrientsToCompare;

@end

@implementation FoodInfo
static NSString *nutrientInfoApi = @"http://matapi.se/nutrient/";

-(DataManager*)dataManager {
    if (!_dataManager) {
        _dataManager = [DataManager getInstance];
    }
    return _dataManager;
}
-(NSArray*)nutrientInfo{
    if (!_nutrientInfo) {
        _nutrientInfo = @[].mutableCopy;
    }
    return _nutrientInfo;
}

-(NSString*)quickInfoType {
    if (!_quickInfoType) {
        _quickInfoType = @"energyKcal";
    }
    return _quickInfoType;
}

-(NSDictionary*)activeQuickTypeDictionary {
    NSPredicate *active = [NSPredicate predicateWithFormat:@"slug == %@", self.quickInfoType];
    NSArray *array = [self.nutrientInfo filteredArrayUsingPredicate:active];
    if (!array.count) {
        return @{ @"slug" : @"energyKcal", @"name" : @"Energy kCal", @"unit" : @"kcal" };
    }
    return array[0];
}

-(NSDictionary*)getFoodInfoForSlug:(NSString*)slug {
    NSPredicate *result = [NSPredicate predicateWithFormat:@"slug == %@", slug];
     NSArray *array = [self.nutrientInfo filteredArrayUsingPredicate:result];
    return array[0];
}

-(NSInteger)numberOfBars {
    return 2*self.nutrientsToCompare.count;
}

-(NSArray*)nutrientsToCompare {
    if (!_nutrientsToCompare) {
        _nutrientsToCompare = @[[self getFoodInfoForSlug:@"carbohydrates"], [self getFoodInfoForSlug:@"fat"], [self getFoodInfoForSlug:@"protein"]];
    }
    return _nutrientsToCompare;
}

-(NSMutableArray*)foodsToCompare {
    if (!_foodsToCompare) {
        _foodsToCompare = @[].mutableCopy;
    }
    return _foodsToCompare;
}

-(void)loadNutrientsInfo {
    NSURL *url = [NSURL URLWithString:nutrientInfoApi];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if (error) {
            return;
        }
        
        NSError *parseError;
        NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        dispatch_async(dispatch_get_main_queue(), ^{
            for (NSDictionary *dict in json){
                [self.nutrientInfo addObject:dict];
            }
        });
    }];
    [task resume];
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.nutrientInfo forKey:@"foodInfoNutrientInfo"];
    [aCoder encodeObject:self.quickInfoType forKey:@"foodInfoQuickInfoType"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    
    if (self) {
        self.nutrientInfo = [aDecoder decodeObjectForKey:@"foodInfoNutrientInfo"];
        self.quickInfoType = [aDecoder decodeObjectForKey:@"foodInfoQuickInfoType"];
    }
    return self;
}

@end
