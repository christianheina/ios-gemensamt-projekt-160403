//
//  CustomFoodTableCell.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-06.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFoodTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *foodNameText;

@end
