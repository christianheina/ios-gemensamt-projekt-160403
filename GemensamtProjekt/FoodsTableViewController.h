//
//  FoodsTableViewController.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-24.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodsTableViewController : UITableViewController <UISearchResultsUpdating>

@end
