//
//  Food.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-03.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "Food.h"
#import "DataManager.h"
#import "UIKit/UIImage.h"

@interface Food ()
@property (nonatomic) DataManager *dataManager;
@property (nonatomic) NSNumber *index;
@property (nonatomic) NSString *name;
@property (nonatomic) NSDictionary *nutrients;

@end

@implementation Food
const NSString *foodApi = @"http://matapi.se/foodstuff/";

-(DataManager*)dataManager {
    if (!_dataManager) {
        _dataManager = [DataManager getInstance];
    }
    return _dataManager;
}

-(instancetype)initWithName:(NSString*)name andNumber:(NSNumber*)index {
    self = [super init];
    
    if (self) {
        self.name = name;
        self.index = index;
    }
    
    return self;
}

-(void)getDataFromApi:(long)priorityIdentifier {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", foodApi, self.index]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if (error) {
            return;
        }
        
        NSError *parseError;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        dispatch_async(dispatch_get_global_queue(priorityIdentifier, 0), ^{
            self.nutrients = json[@"nutrientValues"];
        });
    }];
    [task resume];
}

-(void)clearNutrientData {
    self.nutrients = nil;
}

-(void)checkName:(NSString*)nameToCompare {
    if (![self.name isEqualToString:nameToCompare]) {
        self.name = nameToCompare;
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.index forKey:@"foodIndex"];
    [aCoder encodeObject:self.name forKey:@"foodName"];
    [aCoder encodeBool:self.isFavourite forKey:@"foodFavourite"];
    [aCoder encodeObject:self.nutrients forKey:@"foodNutrients"];
    [aCoder encodeObject:UIImagePNGRepresentation(self.foodImage) forKey:@"foodImage"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    
    if (self) {
        self.index = [aDecoder decodeObjectForKey:@"foodIndex"];
        self.name = [aDecoder decodeObjectForKey:@"foodName"];
        self.isFavourite = [aDecoder decodeBoolForKey:@"foodFavourite"];
        self.nutrients = [aDecoder decodeObjectForKey:@"foodNutrients"];
        self.foodImage = [UIImage imageWithData:[aDecoder decodeObjectForKey:@"foodImage"]];
    }
    return self;
}

@end
