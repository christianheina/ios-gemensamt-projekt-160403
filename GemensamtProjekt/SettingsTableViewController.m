//
//  SettingsTableViewController.m
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-05.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "DataManager.h"

@interface SettingsTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *quickTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *updateIntervalLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastUpdateDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveIntervalLabel;

@property (nonatomic) DataManager *dataManager;

@end

@implementation SettingsTableViewController

-(DataManager*)dataManager {
    if (!_dataManager) {
        _dataManager = [DataManager getInstance];
    }
    return _dataManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self displayUserInfo];
}

-(void)displayUserInfo {
    self.updateIntervalLabel.text = self.dataManager.settings.activeUpdateDictionary[@"name"];
    self.quickTypeLabel.text = [NSString stringWithFormat:@"%@ (%@)", self.dataManager.foodInfo.activeQuickTypeDictionary[@"name"], self.dataManager.foodInfo.activeQuickTypeDictionary[@"unit"]];
    NSString *dateString = [NSDateFormatter localizedStringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.dataManager.lastApiUpdate]
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    self.lastUpdateDateLabel.text = dateString;
    self.saveIntervalLabel.text = self.dataManager.settings.activeSaveDictionary[@"name"];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self showSheetWithTitle:@"Quick info for foods"
                         message:[NSString stringWithFormat:@"Current: %@", self.dataManager.foodInfo.activeQuickTypeDictionary[@"name"]]
             arrayWithDictionary:self.dataManager.foodInfo.nutrientInfo
                buttonTitleBlock:^(NSDictionary *dict) {
                    return [NSString stringWithFormat:@"%@ (%@)", dict[@"name"], dict[@"unit"]];
                }
                withConfirmBlock:^(NSDictionary *dict){
                    self.dataManager.foodInfo.quickInfoType = dict[@"slug"];
                    [self displayUserInfo];
                    [self.dataManager saveFoodInfoData];
                }];
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [self showSheetWithTitle:@""
                             message:@""
                 arrayWithDictionary:self.dataManager.settings.updateIntervals
                    buttonTitleBlock:^NSString *(NSDictionary *dict) {
                        return dict[@"name"];
                    }
                    withConfirmBlock:^(NSDictionary *dict) {
                        self.dataManager.settings.daysBetweenUpdate = [dict[@"value"] intValue];
                        [self displayUserInfo];
                        [self.dataManager saveSettingsData];
                        [self.dataManager checkIfTimeToUpdate];
                    }];
        } else if (indexPath.row == 1) {
            [self.dataManager updateDataFromApi:CFAbsoluteTimeGetCurrent()];
            [self.dataManager resetFoodsNutrient];
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            [self showSheetWithTitle:@"Save interval"
                             message:@"Pick the time between saves"
                 arrayWithDictionary:self.dataManager.settings.saveIntervals
                    buttonTitleBlock:^NSString *(NSDictionary *dict) {
                        return dict[@"name"];
                    }
                    withConfirmBlock:^(NSDictionary *dict) {
                        self.dataManager.settings.secondsBetweenSaves = [dict[@"value"] intValue];
                        [self displayUserInfo];
                        [self.dataManager saveSettingsData];
                    }];
        } else if (indexPath.row == 1) {
            [self.dataManager saveFoodsData];
            [self.dataManager saveFoodInfoData];
        }

    } else if (indexPath.section == 3) {
        [self showAlertWithTitle:@"Delete bookmarks"
                         message:@"Are you sure you want to delete all bookmarks?"
                withConfirmBlock:^(UIAlertAction *action) {
                    [self.dataManager clearBookmarks];
                    [self.dataManager saveFoodsData];
                }];
    } else if (indexPath.section == 4) {
        [self showAlertWithTitle:@"Delete photos"
                         message:@"Are you sure you want to delete all photos?"
                withConfirmBlock:^(UIAlertAction *action) {
                    [self.dataManager clearPhotos];
                    [self.dataManager saveFoodsData];
                }];
    } else if (indexPath.section == 5) {
        [self performSegueWithIdentifier:@"showAboutViewSegue" sender:self];
    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(selectedEnd:) userInfo:cell repeats:NO];
}

-(void)selectedEnd:(NSTimer*)aTimer {
    UITableViewCell *cell = aTimer.userInfo;
    cell.selected = NO;
}

typedef void (^buttonAction)(UIAlertAction *action);
typedef void (^actionUsingDictionary)(NSDictionary *dict);
typedef NSString *(^textFromDictionary)(NSDictionary *dict);

-(void)showSheetWithTitle:(NSString*)title
                  message:(NSString*)message
      arrayWithDictionary:(NSArray*)array
         buttonTitleBlock:(textFromDictionary)buttonTitle
         withConfirmBlock:(actionUsingDictionary)confirmBlock {
    UIAlertController *sheet = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (NSDictionary *dict in array) {
        UIAlertAction *button = [UIAlertAction actionWithTitle:buttonTitle(dict)
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action){
                                                           confirmBlock(dict);
                                                       }];
        [sheet addAction:button];
    }
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [sheet addAction:cancelButton];
    [self presentViewController:sheet animated:YES completion:nil];
}

-(void)showAlertWithTitle:(NSString*)title
                  message:(NSString*)message
         withConfirmBlock:(buttonAction)confirmBlock {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:confirmBlock];
    [alert addAction:confirm];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
