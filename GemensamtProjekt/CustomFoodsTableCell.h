//
//  CustomFoodsTableCell.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-24.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFoodsTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *foodImage;
@property (weak, nonatomic) IBOutlet UITextView *nameText;
@property (weak, nonatomic) IBOutlet UILabel *healthyValue;

@end
