//
//  CustomNutrientsTableCell.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-02-25.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNutrientsTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nutrientNameText;
@property (weak, nonatomic) IBOutlet UILabel *nutrientValueText;


@end
