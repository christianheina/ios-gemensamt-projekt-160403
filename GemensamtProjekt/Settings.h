//
//  Settings.h
//  GemensamtProjekt
//
//  Created by Christian on 2016-03-04.
//  Copyright © 2016 Christian Heina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject <NSCoding>
// Update interval
@property (nonatomic) long updateIntervalMilliseconds;
@property (nonatomic) NSInteger daysBetweenUpdate;
@property (nonatomic, readonly) NSArray *updateIntervals;
@property (nonatomic, readonly) NSDictionary *activeUpdateDictionary;

// Local Save interval
@property (nonatomic) NSInteger secondsBetweenSaves;
@property (nonatomic, readonly) NSArray *saveIntervals;
@property (nonatomic, readonly) NSDictionary *activeSaveDictionary;

@end
